package main

import (
	"fmt"
	"github.com/robaho/fixed"
	"math"
	"math/big"
)

type mullerError struct {
	x float64
}

func (e mullerError) Error() string {
	return fmt.Sprintf("ERROR ***** We've gone off the rails! *****: %g", e.x)
}

func muller(y, z float64) float64 {
	return 108 - (815-1500/z)/y
}

var f1500 = fixed.NewF(1500)
var f815 = fixed.NewF(815)
var f108 = fixed.NewF(108)

const epsilon = .52942

func printHeader() {
	fmt.Println("  Muller's Recurrence - roundoff gone wrong")
	fmt.Println("  108 - (815-1500/z)/y")
	fmt.Println("  https://latkin.org/blog/2014/11/22/mullers-recurrence-roundoff-gone-wrong/\n")
	fmt.Println(" i | naïve floating point | robaho/fixed         | math/big             | stable algorithm ")
	fmt.Println("---|----------------------|----------------------|----------------------|---------------------")
}

func mullerfxt(y, z fixed.Fixed) fixed.Fixed {
	a := f1500.Div(z)
	b := f815.Sub(a)
	c := b.Div(y)
	num := f108.Sub(c)
	return num
}

var b1500 = big.NewRat(1500, 1)
var b815 = big.NewRat(815, 1)
var b108 = big.NewRat(108, 1)

func mullerbig(y, z *big.Rat) *big.Rat {
	t := new(big.Rat)
	t.Quo(b1500, z)
	t.Sub(b815, t)
	t.Quo(t, y)
	t.Sub(b108, t)
	return t
}

func mullerstab(y, z float64) float64 {
	return 8 - 15/y
}

func checkDelta(n float64) bool {
	return math.Abs(5-n) > epsilon
}

func fbig() []*big.Rat {
	x := []*big.Rat{big.NewRat(4, 1), big.NewRat(425, 100)}
	for i := 1; i < iterations; i++ {
		xn := mullerbig(x[i], x[i-1])
		x = append(x, xn)
	}
	return x
}

func fxt() []fixed.Fixed {
	x := []fixed.Fixed{fixed.NewF(4), fixed.NewF(4.25)}
	for i := 1; i < iterations; i++ {
		xn := mullerfxt(x[i], x[i-1])
		x = append(x, xn)
	}
	return x
}

func fp() []float64 {
	x := []float64{4, 4.25}
	for i := 1; i < iterations; i++ {
		xn := muller(x[i], x[i-1])
		x = append(x, xn)
	}
	return x
}

func stab() []float64 {
	x := []float64{4, 4.25}
	for i := 0; i < iterations; i++ {
		xn := mullerstab(x[i+1], x[i])
		x = append(x, xn)
	}
	return x
}

const iterations = 72

func allInOne() {
	xstab := stab()
	xfp := fp()
	xfxt := fxt()
	xbig := fbig()
	printHeader()
	for i := 0; i < iterations; i++ {
		fmt.Printf("%2d | %#20.18g | %#20.18g | %s | %#20.18g\n", i, xfp[i], xfxt[i].Float(), xbig[i].FloatString(18), xstab[i])
	}
}
func main() {
	allInOne()
}
