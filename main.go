package main

import (
	"fmt"
)

func main() {
	fmt.Println("Floating point representation innaccuracies")
	fmt.Println("See https://0.30000000000000004.com/")
	fmt.Printf("printf 1.2+1.7 to 54 decimal places: %.54f\n", 1.2+1.7)
	var a float64 = 1.2
	var b float64 = 1.7
	fmt.Println(a + b)
	var total float64 = 0.0
	for i := 0; i < 79; i++ {
		total = total + a + b
	}
	fmt.Println("println total", total)
	fmt.Println("println total + .11", total+.11)
	fmt.Printf("printf total to 54 places: %.54f\n", total)
	fmt.Printf("printf total * 9 to 54 places: %.54f\n", total*9)

	// http://fit.c2.com/wiki.cgi?SimpleExample
	// note that the expected values from there assume 32-bit float
	// float64 gives different results
	division := [][]float32{
		{1000, 10, 100.0000},
		{-1000, 10, -100.0000},
		{1000, 7, 142.85715},
		{1000, .00001, 100000000},
		{4195835, 3145729, 1.3338196},
	}

	for _, ex := range division {
		fmt.Printf("%4.0f / %4.0f = %f expected: %f\n", ex[0], ex[1], ex[2], ex[0]/ex[1])
	}
	fmt.Printf("1000 / 10: %f\n", float64(1000/10))
	fmt.Printf("-1000 / 10: %f\n", float64(-1000/10))
	fmt.Printf("1000 / 7: %.5f\n", float32(1000)/float32(7))
	fmt.Printf("1000 / .0001: %f\n", float64(1000)/.00001)
	fmt.Printf("4195835 / 3145729: %.6f\n", float32(4195835)/float32(3145729))
}
