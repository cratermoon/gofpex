module cratermoon/gofpex

go 1.18

require github.com/robaho/fixed v0.0.0-20211205151907-ef6645865188

require (
	github.com/mattn/go-sqlite3 v1.14.14 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
)
