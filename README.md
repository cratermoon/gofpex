Go language demonstration of the normal and expected inaccuracies of [IEEE-754](https://ieeexplore.ieee.org/document/30711/) floating point representation

Also including a demonstration of the curious case of Muller's Recurrence.

```math
f(y, z) = 108 - \frac{815 - 1500/z}{y}
```

where $`x_0 = 4`$ and $`x_1 = 4.25`$

Compute $`x_i = f(x_{i-1}, x_{i-2})`$
for
$`x_{30}`$

It turns out this function is discontinuous at $`x_0`$ and $`x_1`$

It's supposed to converge to 5, but naive floating point math will run a few iterations and then converge to 100.


See for background:
* [0.30000000000000004: Floating Point Math](https://0.30000000000000004.com/)
* [What Every Computer Scientist Should Know About Floating-Point Arithmetic](https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html)
* [Muller's Recurrence](https://scipython.com/blog/mullers-recurrence/)
* [Muller's Recurrence - roundoff gone wrong](https://latkin.org/blog/2014/11/22/mullers-recurrence-roundoff-gone-wrong/)
